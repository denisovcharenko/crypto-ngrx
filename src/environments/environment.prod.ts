export const environment = {
  production: true,
  apiUrl: 'https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP&tsyms=UAH,RUB,USD'
};
