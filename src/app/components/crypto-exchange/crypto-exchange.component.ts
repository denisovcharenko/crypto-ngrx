import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ICryptoModel, CurrencyEnum, CryptocurrencyEnum, CryptoModel } from '../../models/crypto.model';
import { AppState } from '../../app.state';
import { GetCryptos } from 'src/app/store/crypto.actions';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as Constants from 'src/app/shared/common/Constants';
import { Observable } from 'rxjs/internal/Observable';



@Component({
  selector: 'app-crypto-exchange',
  templateUrl: './crypto-exchange.component.html',
  styleUrls: ['./crypto-exchange.component.scss']
})


export class CryptoExchangeComponent implements OnInit {

  crypto: Observable<ICryptoModel>;
  form: FormGroup;

  currency: CurrencyEnum;
  cryptocurrencyName: CryptocurrencyEnum;

  currencyArr: Array<CurrencyEnum> = [];
  cryptoCurrencyArr: Array<CryptocurrencyEnum> = [];

  private cryptocurrency: CryptoModel;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.dispatch(new GetCryptos());
    this.crypto = this.store.select('crypto');
    // this.store.select('crypto').subscribe((data) => this.crypto = data );
    this.setCurrencyArr();
    this.setCryptocurrencyArr();
    this.form = new FormGroup({
     count: new FormControl('', [Validators.required, Validators.pattern(Constants.onlyNumberReg)])
    });
  }

  get result(): number {
    const countCntr = this.form.controls.count;
    if (!this.currency || !this.cryptocurrency || !countCntr.valid) {
      return 0;
    }
    const result = this.getCurrencyByСryptocurrency() * this.form.controls.count.value;
    return result;
  }

  get countIsNotValid(): boolean {
    return !this.form.controls.count.valid;
  }


  selectCrypt(value: CryptoModel) {
    this.cryptocurrency = value;
  }

  setCryptocurrencyArr() {
    this.cryptoCurrencyArr = Object.keys(CryptocurrencyEnum).map(value => CryptocurrencyEnum[value]);
  }

  selectCurrency(value: CurrencyEnum) {
    this.currency = value;
  }

  selectCryptocurrency(value: CryptocurrencyEnum) {
    this.cryptocurrencyName = value;
  }

  private getCurrencyByСryptocurrency(): number {
    switch (this.currency) {
      case CurrencyEnum.RUB: return this.cryptocurrency.RUB;
      case CurrencyEnum.UAH: return this.cryptocurrency.UAH;
      case CurrencyEnum.USD: return this.cryptocurrency.USD;
    }
    return 0;
  }

  private setCurrencyArr() {
      this.currencyArr = Object.keys(CurrencyEnum).map(value =>  CurrencyEnum[value]);
  }

}
