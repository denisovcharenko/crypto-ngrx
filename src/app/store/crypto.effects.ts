import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import { CryptoCompareService } from '../services/crypto-compare.service';
import {CryptoActions, GET_CRYPTOS, GetCryptosSuccess} from './crypto.actions';
import { GetCryptos } from './crypto.actions';
import { of } from 'rxjs';

@Injectable()
export class CryptosEffects {
  @Effect()
  getCryptos$ = this.actions$.pipe(
    ofType<GetCryptos>(GET_CRYPTOS),
    mergeMap(() =>
      this.cryptosService.getPrices().pipe(
        map(cryptos => new GetCryptosSuccess(cryptos))
      )
    )
  );

  constructor(private actions$: Actions, private cryptosService: CryptoCompareService) {}
}
