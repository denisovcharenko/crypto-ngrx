import {Action} from '@ngrx/store';
import {ICryptoModel} from '../models/crypto.model';
import {CryptoActions, GET_CRYPTOS_SUCCESS} from './crypto.actions';

const initialState: ICryptoModel = {
  BTC: null,
  ETH: null,
  XRP: null
};

export function reducer(state = initialState, action: CryptoActions) {
  switch (action.type) {
    case GET_CRYPTOS_SUCCESS:
      return {
        ...state,
        BTC: action.payload.BTC,
        ETH: action.payload.ETH,
        XRP: action.payload.XRP
      };
    default:
      return state;
  }
}
