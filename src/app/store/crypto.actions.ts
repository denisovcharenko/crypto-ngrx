 import {Injectable} from '@angular/core';
 import {Action} from '@ngrx/store';
 import {ICryptoModel} from '../models/crypto.model';

 export const GET_CRYPTOS = '[CRYPTOS] GET';
 export const GET_CRYPTOS_SUCCESS = '[CRYPTOS] GET SUCCESS';

 export class GetCryptos implements Action {
   readonly type = GET_CRYPTOS;
 }

 export class GetCryptosSuccess implements Action {
   readonly type = GET_CRYPTOS_SUCCESS;

   constructor(public payload: ICryptoModel) {}
 }

 export type CryptoActions = GetCryptos | GetCryptosSuccess;
