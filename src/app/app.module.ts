import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/cryptos.reducer';
import { CryptoExchangeComponent } from './components/crypto-exchange/crypto-exchange.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CryptoCompareService } from './services/crypto-compare.service';
import { EffectsModule } from '@ngrx/effects';
import { CryptosEffects } from './store/crypto.effects';
import { environment } from 'src/environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



@NgModule({
  declarations: [
    AppComponent,
    CryptoExchangeComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({crypto: reducer}),
    EffectsModule.forRoot([CryptosEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CryptoCompareService],
  bootstrap: [AppComponent]
})
export class AppModule { }
