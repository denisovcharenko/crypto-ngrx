import { ICryptoModel } from './models/crypto.model';

export interface AppState {
  readonly cryptos: ICryptoModel;
}
