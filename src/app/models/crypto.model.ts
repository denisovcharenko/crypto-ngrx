export interface ICryptoModel {
  BTC: CryptoModel;
  ETH: CryptoModel;
  XRP: CryptoModel;
}

export class CryptoModel {
  RUB: number;
  UAH: number;
  USD: number;
}

export enum CurrencyEnum {
  UAH = 'UAH',
  RUB = 'RUB',
  USD = 'USD'
}
export enum CryptocurrencyEnum {
  BTC = 'BTC',
  ETH = 'ETH',
  XRP = 'XRP'
}
