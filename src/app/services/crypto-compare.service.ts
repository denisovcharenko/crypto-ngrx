import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICryptoModel } from '../models/crypto.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CryptoCompareService {

  constructor(private http: HttpClient) { }

  public getPrices(): Observable<ICryptoModel> {
    return this.http.get<ICryptoModel>(environment.apiUrl);
  }
}
